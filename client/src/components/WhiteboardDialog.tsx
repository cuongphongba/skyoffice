import { Web3ReactProvider } from '@web3-react/core'
import styled from 'styled-components'
import IconButton from '@mui/material/IconButton'
import CloseIcon from '@mui/icons-material/Close'
import { useAppDispatch } from '../hooks'
import { closeWhiteboardDialog } from '../stores/WhiteboardStore'
import { getProvider } from '../utils/provider'
import { ActivateDeactivate } from './dapp/ActivateDeactivate'
import { Greeter } from './dapp/Greeter'
import { SectionDivider } from './dapp/SectionDivider'
import { SignMessage } from './dapp/SignMessage'
import { WalletStatus } from './dapp/WalletStatus'

const Backdrop = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  overflow: hidden;
  padding: 8%;
`
const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  background: white;
  border-radius: 16px;
  padding: 16px;
  position: relative;
  display: flex;
  flex-direction: column;

  .close {
    color: black;
    position: absolute;
    top: 16px;
    right: 16px;
  }
`

const WhiteboardWrapper = styled.div`
  display: grid;
  grid-gap: 20px;
`

export default function WhiteboardDialog() {
  const dispatch = useAppDispatch()

  return (
    <Web3ReactProvider getLibrary={getProvider}>
      <Backdrop>
        <Wrapper>
          <IconButton
            aria-label="close dialog"
            className="close"
            onClick={() => dispatch(closeWhiteboardDialog())}
          >
            <CloseIcon />
          </IconButton>
          <WhiteboardWrapper>
            <ActivateDeactivate />
            <SectionDivider />
            <WalletStatus />
            <SectionDivider />
            <SignMessage />
            <SectionDivider />
            <Greeter />
          </WhiteboardWrapper>
        </Wrapper>
      </Backdrop>
    </Web3ReactProvider>
  )
}
