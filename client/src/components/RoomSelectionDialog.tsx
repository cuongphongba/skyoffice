import React, { useState } from 'react'
import logo from '../assets/logo.png'
import styled from 'styled-components'
import Button from '@mui/material/Button'
import IconButton from '@mui/material/IconButton'
import Tooltip from '@mui/material/Tooltip'
import LinearProgress from '@mui/material/LinearProgress'
import Alert from '@mui/material/Alert'
import Snackbar from '@mui/material/Snackbar'
import HelpOutlineIcon from '@mui/icons-material/HelpOutline'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import Web3 from 'web3'
import Web3Modal from 'web3modal'
import { CustomRoomTable } from './CustomRoomTable'
import { CreateRoomForm } from './CreateRoomForm'
import { useAppDispatch, useAppSelector } from '../hooks'
import Bootstrap from '../scenes/Bootstrap'
import phaserGame from '../PhaserGame'
import Game from '../scenes/Game'
import { setLoggedIn, setWallet } from '../stores/UserStore'
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@mui/material'

const Backdrop = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  display: flex;
  flex-direction: column;
  gap: 60px;
  align-items: center;
`

const Wrapper = styled.div`
  background: #222639;
  border-radius: 16px;
  padding: 36px 60px;
  box-shadow: 0px 0px 5px #0000006f;
`

const CustomRoomWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  gap: 20px;
  align-items: center;
  justify-content: center;

  .tip {
    font-size: 18px;
  }
`

const BackButtonWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
`

const Title = styled.h1`
  font-size: 24px;
  color: #eee;
  text-align: center;
`

const Content = styled.div`
  display: flex;
  flex-direction: column;
  gap: 20px;
  margin: 20px 0;
  align-items: center;
  justify-content: center;

  img {
    border-radius: 8px;
    height: 120px;
  }
`

const ProgressBarWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  h3 {
    color: #33ac96;
  }
`

const ProgressBar = styled(LinearProgress)`
  width: 360px;
`

export default function RoomSelectionDialog() {
  const dispatch = useAppDispatch()
  const [showCustomRoom, setShowCustomRoom] = useState(false)
  const [showCreateRoomForm, setShowCreateRoomForm] = useState(false)
  const [showSnackbar, setShowSnackbar] = useState(false)
  const [dialog, setDialog] = useState({ open: false, message: '' })
  const lobbyJoined = useAppSelector((state) => state.room.lobbyJoined)

  const loginWallet = async () => {
    try {
      const providerOptions = {}
      const web3Modal = new Web3Modal({
        cacheProvider: false,
        providerOptions,
      })
      // Check if ethereum wallet exists, if not show notification
      if (window.ethereum) {
        const provider = await web3Modal.connect()
        const web3 = new Web3(provider)
        const accounts = await web3.eth.getAccounts()

        if (!!accounts) {
          dispatch(setLoggedIn(true))
          connect(true)
          getWalletInfo(accounts)
        }
      } else {
        toggleDialog(
          'Not set ethereum wallet or invalid. You need to install Metamask extension, wait for it done and then refresh this page again to make sure it work correctly'
        )
      }
    } catch (error: any) {
      toggleDialog(
        'You have to login MetaMask extension first and then refresh this page again to make sure it work correctly'
      )
    }
  }

  const getWalletInfo = (accounts: string[]) => {
    const web3 = new Web3(window.ethereum)
    let promises = [] as Promise<string>[]

    // Add getBalance request into the list of promises to call parallel
    accounts.forEach((account) => {
      promises.push(web3.eth.getBalance(account))
    })

    // Wait for all promises complete
    Promise.all(promises).then((results) => {
      let accountInfoList = [] as any
      results.forEach((r, index) =>
        accountInfoList.push({ address: accounts[index], balance: web3.utils.fromWei(r) })
      )
      dispatch(setWallet({ accounts: accountInfoList }))
    })
  }

  const connect = (loggedInWallet?: boolean) => {
    if (lobbyJoined) {
      const bootstrap = phaserGame.scene.keys.bootstrap as Bootstrap
      bootstrap.network
        .joinOrCreatePublic()
        .then(() => {
          bootstrap.launchGame()
          if (loggedInWallet) login()
        })
        .catch((error) => console.error(error))
    } else {
      setShowSnackbar(true)
    }
  }

  const login = () => {
    // Set timeout to make sure myPlayer property have enough time to set up after launchGame calls
    setTimeout(() => {
      let game = phaserGame.scene.keys.game as Game
      game.registerKeys()
      game.myPlayer.setPlayerName('Player 1')
      game.myPlayer.setPlayerTexture('adam')
      game.network.readyToConnect()
    }, 500)
  }

  const toggleDialog = (message?: string | undefined) => {
    setDialog({ ...dialog, open: !dialog.open, message: message ?? '' })
  }

  return (
    <>
      <Dialog open={dialog.open} onClose={() => toggleDialog()}>
        <DialogTitle style={{ color: 'white' }}>Notification</DialogTitle>
        <DialogContent>
          <DialogContentText>{dialog.message}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => toggleDialog()}>Close</Button>
        </DialogActions>
      </Dialog>
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        open={showSnackbar}
        autoHideDuration={3000}
        onClose={() => {
          setShowSnackbar(false)
        }}
      >
        <Alert
          severity="error"
          variant="outlined"
          // overwrites the dark theme on render
          style={{ background: '#fdeded', color: '#7d4747' }}
        >
          Trying to connect to server, please try again!
        </Alert>
      </Snackbar>
      <Backdrop>
        <Wrapper>
          {showCreateRoomForm ? (
            <CustomRoomWrapper>
              <Title>Create Custom Room</Title>
              <BackButtonWrapper>
                <IconButton onClick={() => setShowCreateRoomForm(false)}>
                  <ArrowBackIcon />
                </IconButton>
              </BackButtonWrapper>
              <CreateRoomForm />
            </CustomRoomWrapper>
          ) : showCustomRoom ? (
            <CustomRoomWrapper>
              <Title>
                Custom Rooms
                <Tooltip
                  title="We update the results in realtime, no refresh needed!"
                  placement="top"
                >
                  <IconButton>
                    <HelpOutlineIcon className="tip" />
                  </IconButton>
                </Tooltip>
              </Title>
              <BackButtonWrapper>
                <IconButton onClick={() => setShowCustomRoom(false)}>
                  <ArrowBackIcon />
                </IconButton>
              </BackButtonWrapper>
              <CustomRoomTable />
              <Button
                variant="contained"
                color="secondary"
                onClick={() => setShowCreateRoomForm(true)}
              >
                Create new room
              </Button>
            </CustomRoomWrapper>
          ) : (
            <>
              <Title>Welcome to SkyOffice</Title>
              <Content>
                <img src={logo} alt="logo" />
                <Button variant="contained" onClick={loginWallet}>
                  Login
                </Button>
                <Button variant="contained" color="secondary" onClick={() => connect()}>
                  Connect to public lobby
                </Button>
                <Button
                  variant="outlined"
                  color="secondary"
                  onClick={() => (lobbyJoined ? setShowCustomRoom(true) : setShowSnackbar(true))}
                >
                  Create/find custom rooms
                </Button>
              </Content>
            </>
          )}
        </Wrapper>
        {!lobbyJoined && (
          <ProgressBarWrapper>
            <h3> Connecting to server...</h3>
            <ProgressBar color="secondary" />
          </ProgressBarWrapper>
        )}
      </Backdrop>
    </>
  )
}
