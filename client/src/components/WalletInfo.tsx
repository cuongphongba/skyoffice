// import React, { useState } from 'react'
import { List, ListItem, ListItemAvatar, Avatar, ListItemText, Typography } from '@mui/material'
import AccountBalanceWalletIcon from '@mui/icons-material/AccountBalanceWallet'
import styled from 'styled-components'
import { useAppSelector } from '../hooks'

const Wrapper = styled.div`
  position: fixed;
  top: 10px;
  right: 10px;
  display: flex;
  flex-direction: column;
  color: black !important;
  background: white;
`

export default function WalletInfo() {
  const { accounts } = useAppSelector((state) => state.user.wallet)

  return (
    <>
      {accounts.length > 0 && (
        <Wrapper>
          <List>
            {accounts.map((account: any, index) => {
              return (
                <ListItem key={index}>
                  <ListItemAvatar>
                    <Avatar>
                      <AccountBalanceWalletIcon />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    primary={
                      <Typography variant="subtitle2">
                        <strong>Address:</strong> {account.address}
                      </Typography>
                    }
                    secondary={
                      <Typography variant="subtitle2">
                        <strong>Balance:</strong> {account.balance}
                      </Typography>
                    }
                  />
                </ListItem>
              )
            })}
          </List>
        </Wrapper>
      )}
    </>
  )
}
