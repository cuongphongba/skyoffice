# SkyOffice ![License](https://img.shields.io/badge/license-MIT-blue) ![PRs Welcome](https://img.shields.io/badge/PRs-welcome-green.svg)

You'll need [Node.js](https://nodejs.org/en/), [npm](https://www.npmjs.com/) installed.

## Getting Started

Clone this repository to your local machine:

```bash
git clone https://gitlab.com/cuongphongba/skyoffice.git
```

This will create a folder named `SkyOffice`. You can specify a different folder name like this:

```bash
git clone https://gitlab.com/cuongphongba/skyoffice.git my-folder-name
```

To start a server, go into the project folder and install dependencies/run start command:

```bash
cd SkyOffice or 'my-folder-name'
npm install
npm run start
```

To start a blockchain network, go into blockchain folder and install dependencies/run commands:

```bash
cd SkyOffice/blockchain or 'my-folder-name/blockchain'
yarn install
yarn hardhat compile
yarn hardhat node
```

To start a client, go into the client folder and install dependencies/run start command:

```bash
cd SkyOffice/client or 'my-folder-name/client'
npm install
npm run start
```

After all of these things setup, you need to login to metamask wallet by installing metamask extension for browser and login with the account that you have or you can create new one if it's unavailable, then refresh the SkyOffice page again to make sure it work correctly.

![image](https://i.postimg.cc/26z7vW95/Capture1.png)

![image](https://i.postimg.cc/15Nw8MRH/Capture2.png)

![image](https://i.postimg.cc/Tw45w5ZF/Capture3.png)

The link that I refered to build a React Dapp with Hardhat and MetaMask is https://medium.com/building-blocks-on-the-chain/how-to-build-a-react-dapp-with-hardhat-and-metamask-9cec8f6410d3
